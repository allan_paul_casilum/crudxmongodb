const express = require('express');
const bodyParser= require('body-parser');
const app = express();
const MongoClient = require('mongodb').MongoClient

let _public_path = __dirname;
let db;
let uri = "mongodb://allan:OkSKw1r1qOjIG1Ac@zap-shard-00-00-xxors.mongodb.net:27017,zap-shard-00-01-xxors.mongodb.net:27017,zap-shard-00-02-xxors.mongodb.net:27017/test?ssl=true&replicaSet=zap-shard-0&authSource=admin";

MongoClient.connect(uri, (err, client) => {
  if (err) return console.log(err)
  db = client.db('mydb') // whatever your database name is
})

app.set('view engine', 'ejs')

//middleware
app.use(bodyParser.urlencoded({extended: true}));
//middleware

app.listen(3000, function() {
  console.log('listening on 3000')
});
app.get('/', (req, res) => {
	res.send(_public_path + ' Hello World');
});
app.get('/test', (req, res) => {
	res.send('Test');
});
app.get('/main', (req, res) => {
	res.sendFile(__dirname + '/index.html');
});
app.get('/create', (req, res) => {
	res.sendFile(__dirname + '/create.html');
});
app.post('/quotes', (req, res) => {
	console.log('wahhhh');
	console.log(req.body);
	//res.redirect('/');
});

app.get('/movies', (req, res) => {
  var cursor = db.collection('movie').find().toArray(function(err, results) {
	if (err) return console.log(err)
	// renders index.ejs
	res.render('index.ejs', {movies: results})
  });
  //cursor.close();
})